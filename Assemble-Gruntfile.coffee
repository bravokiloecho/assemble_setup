module.exports = (grunt) ->
  
  # Project configuration.
  grunt.initConfig
    pkg: grunt.file.readJSON("package.json")

    asset_path: 'devAssets/'
    public_path: 'public/'
    dev_path: 'site/'
    view_path: 'views/'
    data_path: 'data/'
    port: '4000'
    liveURL: 'http://yoursite.com'


    assemble:
      options:
        assets: "public"
        engine: 'jade'
        data: ['<%= data_path %>*.yml','<%= data_path %>*.json']
        flatten: false  
        plugins: [ 'assemble-markdown-import' ]
        basedir: "<%= view_path %>"
      
        
      dev:
        options:
          production: false
          staging: false
          prettify: true
        expand: true
        cwd: "views/"
        src: [
          '*.jade'
          '*/*.jade'
          '*/*/*.jade'
          '!_partials/*'
        ]
        dest: "<%= dev_path %>"

      staging:
        options:
          production: false
          staging: true
          prettify: false
        expand: true
        cwd: "views/"
        src: [
          '*.jade'
          '*/*.jade'
          '*/*/*.jade'
          '!_partials/*'
        ]
        dest: 'public/'

      
      public:
        options:
          staging: false
          production: true
          prettify: false
        expand: true
        cwd: "views/"
        src: [
          '*.jade'
          '*/*.jade'
          '*/*/*.jade'
          '!_partials/*'
        ]
        dest: 'public/'

    
    watch:
      sass:
        files: [
          "<%= asset_path %>sass/*"
          "<%= asset_path %>sass/*/*"
        ]
        tasks: ["compass:dev"]
        options:
          atBegin: false

      coffee:
        files: [
          "<%= asset_path %>coffee/*"
        ]
        tasks: ["coffee"]
        options:
          atBegin: false

      assemble:
        files: [
          "<%= view_path %>*"
          "<%= view_path %>*/*"
          "<%= data_path %>*"
          "text/*"
        ]
        tasks: ["assemble:dev"]

      svg:
        files: [
          "<%= asset_path %>svg/*.svg"
          "<%= asset_path %>svg/*/*.svg"
        ]
        tasks: ["svgmin"]
        options:
          atBegin: false

      watchFiles:
        files: [
          "<%= public_path %>js/mouse/*"
          "<%= dev_path %>*"
        ]
        options:
          livereload: true

    browserSync:
      options:
        watchTask: true
        port: "<%= port %>"

      files:
        src: [
          "<%= public_path %>css/mouse/*.css"
          "<%= public_path %>gfx/*"
          "<%= public_path %>gfx/*/*"
          "<%= public_path %>images/*"
        ]

    uglify:
      plugins:
        options:
          mangle: true
          beautify: false
          compress: true

        files:
          "<%= public_path %>js/plugins.js": ["<%= asset_path %>plugins/*.js"]

      
      # combines plugins and scripts
      combine_all:
        options:
          mangle: true
          beautify: false
          compress:
            drop_console: true

        files:
          "<%= public_path %>js/min/scripts.js": [
            "<%= public_path %>js/plugins.js"
            "<%= public_path %>js/mouse/scripts.js"
          ]

    compass:
      dev:
        options:
          sassDir: "<%= asset_path %>sass"
          cssDir: "<%= public_path %>css/mouse"
          # outputStyle: "nested"
          fontsPath: "<%= public_path %>Fonts"
          sourcemap: true
          force: true

      dist:
        options:
          sassDir: "<%= asset_path %>sass"
          cssDir: "<%= public_path %>css/min"
          outputStyle: "compressed"
          fontsPath: "<%= public_path %>Fonts"
          sourcemap: false
          force: true

    coffee:
      compileBare:
        options:
          bare: true
          join: true
          sourceMap: true

        files:
          "<%= public_path %>js/mouse/scripts.js": ["<%= asset_path %>coffee/*.coffee"]


    inline:
      dist:
        src: '<%= public_path %>index.html'


    connect:
      server:
        options:
          port: "<%= port %>"
          hostname: "*"


    imageoptim: # Task
      options:
        quitAfter: true
      images:
        src: ["<%= public_path %>images/"]
      gfx:
        src: ["<%= public_path %>gfx/"]


    image_resize:
      resize:
        options:
          overwrite: true
          upscale: false
          crop: false
          width: 1600
        src: '<%= asset_path %>photos/*.{png,jpg,gif}'
        dest: '<%= public_path %>photos/'

    svgmin:
      options:
        plugins: [
          {
            removeViewBox: false
          }, {
            removeUselessStrokeAndFill: true
          }
        ]
      dist:
        files: [
          expand: true # Enable dynamic expansion
          cwd: "<%= asset_path %>svg/" # Src matches are relative to this path
          src: ["*.svg","*/*.svg"] # Actual patterns to match
          dest: "<%= public_path %>gfx/" # Destination path prefix
        ]

    # Open pages with Grunt
    open:
      dev:
        path: 'http://localhost:<%= port %>/dev'
      live:
        path: '<%= liveURL %>'


    # Deploy to amazon S3
    # Readme: https://github.com/jpillora/grunt-aws
    aws: grunt.file.readJSON '../aws/credentials.json'
    s3:
      options:
        accessKeyId: "<%= aws.accessKeyId %>"
        secretAccessKey: "<%= aws.secretAccessKey %>"
        bucket: "BUCKETNAME.BUCKET.COM"
        region: 'eu-west-1'
      public:
        cwd: "<%= public_path %>"
        src: "**"



  
  # Load grunt plugins.
  grunt.loadNpmTasks "assemble"
  grunt.loadNpmTasks "grunt-contrib-compass"
  grunt.loadNpmTasks "grunt-contrib-watch"
  grunt.loadNpmTasks "grunt-browser-sync"
  grunt.loadNpmTasks "grunt-contrib-uglify"
  grunt.loadNpmTasks "grunt-contrib-coffee"
  grunt.loadNpmTasks "grunt-inline"
  grunt.loadNpmTasks "grunt-contrib-connect"
  grunt.loadNpmTasks "grunt-imageoptim"
  grunt.loadNpmTasks "grunt-image-resize"
  grunt.loadNpmTasks "grunt-svgmin"
  grunt.loadNpmTasks "grunt-open"
  grunt.loadNpmTasks "grunt-aws"

  
  #register tasks
  
  #watch with css inject
  grunt.registerTask "default", [
    "connect"
    "browserSync"
    "watch"
  ]
  
  # compile all files that need compiling
  grunt.registerTask "c", [
    "assemble:dev"
    "assemble:public"
    "compass"
    "coffee"
    "uglify:plugins"
    # "uglify:legacyPlugins"
    "uglify:combine_all"
    "inline"
  ]


  # DEPLOYMENT
  grunt.registerTask "deploy", [
      "assemble:dev"
      "compass"
      "coffee"
      "uglify:plugins"
      # "uglify:legacyPlugins"
      "uglify:combine_all"
      "inline"
      "s3"
  ]


  # COMPILE TASKS
  grunt.registerTask "ass", ["assemble"]
  grunt.registerTask "coff", ["coffee","uglify:combine_all"]
  grunt.registerTask "sass", ["compass"]
  grunt.registerTask "plug", ["uglify:plugins"]
  grunt.registerTask "min", ["imageoptim"]

  # minify SVGs
  grunt.registerTask "svg", ["svgmin"]
