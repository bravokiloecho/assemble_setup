

var fs = require('fs');
var gm = require('gm');
var Glob = require("glob");
var _ = require("lodash");

// LOW DB things
var low = require('lowdb');
var lowDB = 'data/photos.json';
low.path = lowDB;
low.load(lowDB);


function convertSingle (longedge,location,filename,destination,index,arrayName) {

	if (low(arrayName).where({index: index}).size()) {
		console.log('already there');
		
		return;
	}

	gm(location+filename)
	.noProfile()
	.resize(longedge,longedge,'>') // resize long edge to set value
	.stream(function(err,image){
		gm(image)
		.size(function (err,value){			
			
			destination = destination.replace('public/','');
			
			low(arrayName).insert({
				filename: destination+filename,
				width: value.width,
				height: value.height,
				index: index
			});
		})
		.flop()
		.write(destination+filename, function (err) {
			if (!err) {
				console.log('created: '+filename);
				low.db[arrayName] = low(arrayName).sortBy('index').value();
				low.save(lowDB);
			}
			else {
				console.log(err);
			}
		});
	});
}

function convertAllImages (longedge,location,destination,arrayName) {
	
	var images = getAllImages('*.jpg',location);
	var howMany = images.length;

	for ( var i = 0; i < howMany; i++ ) {
		
		var filename = images[i];
		console.log('adding: '+filename+' '+i);
		
		convertSingle(longedge,location,filename,destination,i,arrayName);
	}
}

function getAllImages (pattern,path) {

	var images = new Glob(pattern, {mark: false, sync:true, cwd: path});

	return images;
}

module.exports.convertAllImages = convertAllImages;