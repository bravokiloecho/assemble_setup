var fs = require('fs');
var gm = require('gm');
var Glob = require("glob");

// LOW DB things
var low = require('lowdb');
var lowDB = 'data/photos.json';
low.path = lowDB;


// THE FUNCTION TO RUN
convertAllImages('assets/original_photos/','public/images/flipped/');

function convertSingle (location,filename,destination,index) {

	gm(location+filename)
	.noProfile()
	.resize('1400>') // resize long edge to 1400
	.stream(function(err,image){
		gm(image)
		.size(function (err,value){			
			
			destination = destination.replace('public/','');
	
			low('photos').insert({
				filename: destination+filename,
				width: value.width,
				height: value.height,
				index: index
			});
		})
		.flop()
		.write(destination+filename, function (err) {
			if (!err) {
				console.log('created: '+filename);
				// var sort = low('photos').sortBy('index').save('phots');
			}
			else {
				console.log(err);
			}
		});
	});
}

function convertAllImages (location,destination) {
	
	var images = getAllImages('*.jpg',location);
	var howMany = images.length;

	for (var i =0;i < howMany;i++) {
		var filename = images[i];
		console.log('adding: '+filename+' '+i);
		
		convertSingle(location,filename,destination,i);
	}
}

function getAllImages (pattern,path) {

	var images = new Glob(pattern, {mark: false, sync:true, cwd: path});

	return images;
}